import json
import os
import pwd
import grp
import stat
import subprocess
import csv
import re
import threading
import http.client
import argparse
import platform
import struct 
import socket
import shlex



class Colors:

    PURPLE = '\033[95m'
    DARKCYAN = '\033[36m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    END = '\033[0m'

original_path = os.path.dirname(os.path.abspath(__file__))
headers = {'content-type': 'application/json'}
interesting_facts = []
address = ""
users_SUID = dict()
users_SGID = dict()
xin_pots = dict()


def general_info():

    print(Colors.DARKCYAN, 'General info:', Colors.END)
    name = platform.uname()[1]
    print(name + "'s security check:")
    system = platform.uname()[0]
    release = platform.uname()[2]
    dist = platform.uname()[3]
    machine = platform.uname()[4]
    processor = platform.uname()[5]

    print("Specific System information:", system, release, dist, machine, processor)
    user = pwd.getpwuid(os.getuid())[0]
    iD = str(os.getegid())
    print("User:",  user + ",", "with ID:", iD)

    outscript= {
        'name': name,
        'system': system,
        'release': release,
        'dist': dist,
        'machine': machine,
        'processor': processor,
        'user': user,
        'path': os.environ["PATH"],
        'ID': iD
    }

    try:
        with open((original_path + '/General.json'), 'w') as outfile:
            json.dump(outscript, outfile)
    except PermissionError:
        pass

    post("/Pysec/General", outscript)



def user_info():

    print(Colors.GREEN, "All the users and their information:", Colors.END)


    output = []
    for u in pwd.getpwall():
        user = u.pw_name
        groups = [g.gr_name for g in grp.getgrall() if user in g.gr_mem]
        gid = pwd.getpwnam(user).pw_gid
        groups.append(grp.getgrgid(gid).gr_name)
        UID = u.pw_uid
        HDIR = u.pw_dir
        SUID = []
        SGID = []
        if UID in users_SUID:
            SUID = users_SUID[UID]
        if UID in users_SGID:
            SGID = users_SGID[UID]
        rest = ''.join(["UID:", " " + str(UID), " GID:", " " + str(gid), " HDIR", " " + HDIR, " GROUPS:", " " + str(groups)])

        print(Colors.DARKCYAN, "User:", user, rest, Colors.END)
        out = False
        encrypted = False

        try:
            if '.ecryptfs' in os.listdir(HDIR):
                encrypted = True
        except (PermissionError, FileNotFoundError):
            pass

        sout, serr = subprocess.Popen(['crontab', '-u', user, '-l'], stderr=subprocess.PIPE,
                                      stdout=subprocess.PIPE).communicate()
        if sout:
            s = sout.decode("utf-8")

        else:
            s = serr.decode("utf-8")
        try:
            print("Checking if home directory of", user, "is writable by someone else")
            if (os.stat(u.pw_dir).st_mode & stat.S_IWOTH != 0) or (os.stat(u.pw_dir).st_mode & stat.S_IWGRP != 0):
                out = True
                verbint(Colors.RED, "Home directory of", user, u.pw_dir, "is writable by others", Colors.END)
                interesting_facts.append(''.join(["Home directory of ", user, u.pw_dir, " is writable by others"]))

        except FileNotFoundError:
            pass
        output.append({"User": user, "UID": str(UID), "GID": str(gid), "HDIR": HDIR,
                       "GROUPS": groups, "Writable": out, "Cronjobs": s, "Encrypted": encrypted,
                       "SUID": SUID, "SGID": SGID})

    try:
        print(Colors.DARKCYAN, "Checking for default limits for users (quota)", Colors.END)
        out = (''.join(['Default limits for users:', subprocess.check_output(['repquota', '-a']).decode("utf-8"), '\n']))

        verbint(out)

    except FileNotFoundError:
        print("The program 'repquota' is currently not installed.")
        pass

    try:
        shadow = open('/etc/shadow').read()
        passwd = open('/etc/passwd').read()
    except (FileNotFoundError, PermissionError):
        shadow = ""
        passwd = ""

    outscript = {
        'shadow': shadow,
        'passwd': passwd,
        'users': output
    }

    try:
        with open((original_path + '/User.json'), 'w') as outfile:
            json.dump(outscript, outfile)
    except PermissionError:
        pass

    post('/Pysec/User', outscript)



def kernel_info():
    print(Colors.GREEN, "Kernel information:", Colors.END)
    output = []
    out = "".join([".config file of the current kernel:", os.uname()[2], "\n"])
    print(Colors.DARKCYAN, out, Colors.END)
    output.append(out)

    sec_options = {'CONFIG_CC_STACKPROTECTOR=y': "Stack protector", 'CONFIG_DEBUG_RODATA=y': "Read_only data sections",
                   'CONFIG_DEBUG_SET_MODULE_RONX=y': "Module RO_NX",
                   'CONFIG_DEVKMEM=n': "dev_kmem", 'CONFIG_STRICT_DEVMEM=y': "dev_mem protection",
                   'CONFIG_LSM_MMAP_MIN_ADDR=0': "address_0 protection",
                   'CONFIG_SECCOMP=y': "Syscall Filtering", 'CONFIG_SECURITY_SMACK=y': "SMACK",
                   'CONFIG_KEXEC_CORE=y': 'Block kexec'}
    confi = []
    script = []
    print(Colors.DARKCYAN, "Saving the .config file to a separate json file, and checking the security features")
    try:
        with open(os.path.join('/usr/src/', "linux-headers-" + os.uname()[2], '.config')) as s:
            for line in s:
                script.append(line)
                if line.rstrip() in sec_options.keys():
                    confi.append(sec_options[line.rstrip()])
    except FileNotFoundError:
        pass

    outscript = {
        'name': '.config',
        'output': ''.join(script)
    }

    try:
        with open((original_path + '/Config.json'), 'w') as outfile:
            json.dump(outscript, outfile)
    except PermissionError:
        pass

    post('/Pysec/Config', outscript)


    os.chdir('/etc')
    outscript = []
    for line in confi:
        out = ''.join([line, " is turned on"])
        outscript.append({'name': line, 'output': out})
        print(Colors.DARKCYAN, out, Colors.END)

    for line in sec_options.values():
        if line not in confi:
            out = ''.join([line, " is turned off"])
            outscript.append({'name': line, 'output': out})
            interesting_facts.append(out)
            print(Colors.RED, out, Colors.END)

    if os.path.exists('/sys/firmware/efi'):
        out = "System booted in UEFI"
        print(Colors.DARKCYAN, out, Colors.END)
    else:
        out = "System booted in BIOS"
        interesting_facts.append(out)
        print(Colors.RED, out, Colors.END)
    outscript.append({'name': 'Booted in', 'output': out})

    try:
        with open('/etc/modprobe.d/blacklist.conf', 'r') as f:

            print("The Blacklist.conf file:")
            print(f)
            outscript.append({'name': 'Blacklist.conf', 'output': f.read()})
    except FileNotFoundError:
        out = 'No Blacklist Rare Protocols'
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'Blacklist.conf', 'output': out})

    try:
        with open('/etc/sysconfig/selinux', 'r') as f:

            print("SELinux:")
            print(f)
            outscript.append({'name': 'SELinux', 'output': f.read()})
    except FileNotFoundError:
        out = 'SELinux is not installed'
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'SELinux', 'output': out})

    try:
        b = open('/proc/sys/kernel/kptr_restrict').read()
        if "1" in b:
            out = "Kernel Address Display Restriction is turned on"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "Kernel Address Display Restriction is turned off"
            interesting_facts.append(out)
            print(Colors.RED, out, Colors.END)
        outscript.append({'name': 'Kernel Address Display Restriction', 'output': out})
    except FileNotFoundError:
        out = "Kernel Address Display Restriction is turned off"
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'Kernel Address Display Restriction', 'output': out})

    try:
        b = open('/proc/sys/kernel/dmesg_restrict').read()
        if "1" in b:
            out = "dmesg restrictions are turned on"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "dmesg restrictions are turned off"
            interesting_facts.append(out)
            print(Colors.RED, out, Colors.END)
        outscript.append({'name': 'dmesg restrictions', 'output': out})
    except FileNotFoundError:
        out = "dmesg restrictions are turned off"
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'dmesg restrictions', 'output': out})

    try:
        b = open('/proc/sys/kernel/randomize_va_space').read()

        if "1" in b:
            out = "Conservative address space randomization."
            print(Colors.DARKCYAN, out, Colors.END)
        elif "2" in b:
            out = "Full address space randomization."
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "No address space randomization."
            interesting_facts.append(out)
            print(Colors.RED, out, Colors.END)
        outscript.append({'name': 'Address space randomization', 'output': out})
    except FileNotFoundError:
        out = 'No address space randomization'
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'Address space randomization', 'output': out})

    try:
        b = open('/proc/sys/kernel/yama/ptrace_scope').read()

        if "1" in b:
            out = "Cannot ptrace processes that are not a descendant of the debugger"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "Possible to ptrace processes that are not a descendant of the debugger"
            print(Colors.RED, out, Colors.END)
            interesting_facts.append(out)
        outscript.append({'name': 'Ptrace processes', 'output': out})
    except FileNotFoundError:
        out = 'Ptrace-ing processes that are not a descendant of the debugger is possible'
        output.append(out)
        outscript.append({'name': 'Ptrace processes', 'output': out})
        interesting_facts.append(out)

    try:
        b = open('/proc/sys/kernel/yama/protected_sticky_symlinks').read()

        if "1" in b:
            out = "Symlink restrictions are on"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "Symlink restrictions are off"
            print(Colors.RED, out, Colors.END)
            interesting_facts.append(out)
        outscript.append({'name': 'Symlink restrictions', 'output': out})
    except FileNotFoundError:
        out = 'Symlink restrictions are off'
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'Symlink restrictions', 'output': out})

    try:
        b = open('/proc/sys/kernel/yama/protected_nonaccess_hardlinks').read()

        if "1" in b:
            out = "Hardlink restrictions are on"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "Hardlink restrictions are off"
            print(Colors.RED, out, Colors.END)
            interesting_facts.append(out)
        outscript.append({'name': 'Hardlink restrictions', 'output': out})
    except FileNotFoundError:
        out = 'Hardlink restrictions are off'
        print(Colors.RED, out, Colors.END)
        interesting_facts.append(out)
        outscript.append({'name': 'Hardlink restrictions', 'output': out})

    try:
        b = open('/proc/sys/net/ipv4/tcp_syncookies').read()

        if "1" in b:
            out = "SYN cookies protection on"
            print(Colors.DARKCYAN, out, Colors.END)
        else:
            out = "SYN cookies protection off"
            print(Colors.RED, out, Colors.END)
            interesting_facts.append(out)
        outscript.append({'name': 'SYN cookies protection', 'output': out})
    except FileNotFoundError:
        out = 'SYN cookies protection off'
        print(Colors.RED, out, Colors.END)
        outscript.append({'name': 'SYN cookies protection', 'output': out})
        interesting_facts.append(out)

    sout, serr = subprocess.Popen(['ufw', 'status', 'verbose'], stderr=subprocess.PIPE,
                                  stdout=subprocess.PIPE).communicate()
    if sout:
        s = sout.decode("utf-8")
        outscript.append({'name': 'UncomplicatedFirewall', 'output': s})

    sout, serr = subprocess.Popen(['apparmor_status'], stderr=subprocess.PIPE,
                                  stdout=subprocess.PIPE).communicate()
    if sout:
        s = sout.decode("utf-8")
        outscript.append({'name': 'AppArmor', 'output': s})

    try:
        b = open('/proc/cpuinfo').read()
        outscript.append({'name': 'CPU', 'output': b})
        verbint(Colors.DARKCYAN, b, Colors.END)
    except (FileNotFoundError, PermissionError):
        pass

    software = ""

    for commando in [['dpkg', '-l'], ['rpm', '-qa']]:
        try:
            software = subprocess.check_output(commando).decode("utf-8")
            verbint(software)
        except FileNotFoundError:
            continue

    output = {'general': outscript,
              'software': software}

    try:
        with open((original_path + '/Kernel.json'), 'w') as outfile:
            json.dump(output, outfile)
    except PermissionError:
        pass

    post('/Pysec/Kernel', output)


def processes_info_linux():

    print(Colors.DARKCYAN, "Gathering information about the currently running processes", Colors.END)
    verbint(Colors.GREEN, "List of all the processes:", Colors.END)
    output = []
    pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
    ports = dict()
    data = [['PID', 'Name', 'Owner', 'Path', 'Ports']]

    for path in ['/proc/net/tcp', '/proc/net/udp']:
        for line in open(path).read().split('\n')[1:]:
            if len(line) is 0: continue
            zeile = line.split()
            ports[zeile[9]] = ''.join([str(int(zeile[1].split(":")[1], 16)), " -> ", socket.inet_ntoa(struct.pack("<L", int(zeile[2].split(":")[0], 16)))])

    for pid in pids:

        try:
            owner = ""
            name = ""
            path = ""
            found = True
            for line in open('/proc/' + pid + '/status').read().split('\n'):
                if 'Name:' in line:
                    name = line.split()[1]
                elif 'Uid:' in line:
                    owner = pwd.getpwuid(int(line.split()[1]))[0]
                    path = open('/proc/' + pid + '/cmdline', 'r').read().split('\0')[0]

            if not os.path.exists(str(path)):
                path = "Couldn't find the path"
                found = False
            out = ''.join(['Process', pid + ":", name + ":", path, "with owner:", owner, "open ports, sockets, pipes, etc:"])
            verbint(Colors.DARKCYAN, out, Colors.END)
            files = []
            porten = []
            pipes = set()
            for fds in os.listdir(os.path.join('/proc', pid, 'fd')):
                try:
                    file = os.readlink(os.path.join('/proc', pid, 'fd', fds))

                    if file is '/dev/null':
                        file = None
                except OSError:
                    continue
                verbint(file)

                m = re.search("(socket:\[)(\d+)(\])", file)
                p = re.search("(pipe:\[)(\d+)(\])", file)
                if m and m.group(2) in ports:
                    porten.append(ports[m.group(2)] + " being used by process " + pid + ":" + name)
                    port = ports[m.group(2)].split()[0]
                    if port in xin_pots:
                        data.append([pid, xin_pots[port] + "-xinted.d", owner, path, port])
                    else:
                        data.append([pid, name, owner, path, port])
                elif p:
                    pipes.add(p.group(2))
                else:
                    files.append(file)
            dangerous = "Path not modifiable by non owners"
            if found:
                verbint("Checking if other users besides", owner, "can change", path)
                if (os.stat(path).st_mode & stat.S_IWOTH != 0) or (os.stat(path).st_mode & stat.S_IWGRP != 0):
                    dangerous = ''.join(["Other users besides ", owner, " can change ", path])
                    interesting_facts.append(dangerous)
                    verbint(Colors.RED, dangerous, Colors.END)
            output.append({
                'PID': pid,
                'Owner': owner,
                'Name': name,
                'Path': path,
                'Dangerous': dangerous,
                'Files': files,
                'Ports': porten,
                'Pipes': list(pipes)
                })

        except OSError:
            continue

    try:
        with open(original_path+'/processes.csv', 'w') as fp:
            a = csv.writer(fp, delimiter=',')
            a.writerows(data)
    except PermissionError:
        pass

    try:
        with open((original_path + '/ProcessInfo.json'), 'w') as outfile:
            json.dump(output, outfile)
    except PermissionError:
        pass

    post('/Pysec/ProcessInfo', output)


def dangerous_data(dangerousfiles, maxdepth):
    SUID = []
    World_SUID = []
    SGID = []
    os.chdir('/')
    for dpath, dnames, fnames in os.walk(os.getcwd()):
        if dpath.count(os.path.sep) > maxdepth:
            del dnames[:]
        for i, fname in enumerate([os.path.join(dpath, fname) for fname in fnames]):
            try:
                if os.lstat(fname).st_mode & stat.S_ISUID:
                    orgname = fname
                    id = os.stat(fname).st_uid
                    if os.stat(fname).st_mode & stat.S_IWGRP:
                        orgname += ", group writable"
                    if os.stat(fname).st_mode & stat.S_IWOTH:
                        World_SUID.append(fname + " -owned by " + pwd.getpwuid(id).pw_name)
                        orgname += ", world writable"

                    SUID.append(fname + " -owned by " + pwd.getpwuid(id).pw_name)
                    if id in users_SUID:
                        users_SUID[id].append(orgname)
                    else:
                        users_SUID[id] = [orgname]

                elif os.lstat(fname).st_mode & stat.S_ISGID:
                    orgname = fname
                    id = os.stat(fname).st_uid

                    if os.stat(fname).st_mode & stat.S_IWGRP:
                        orgname += ", group writable"
                    if os.stat(fname).st_mode & stat.S_IWOTH:
                        orgname += ", world writable"

                    SGID.append(fname + " -owned by " + pwd.getpwuid(id).pw_name)
                    if id in users_SGID:
                        users_SGID[id].append(orgname)
                    else:
                        users_SGID[id] = [orgname]

            except (PermissionError, FileNotFoundError):
                continue
    dangerousfiles.append({'name': 'SUID files', 'outputs': SUID})
    dangerousfiles.append({'name': 'SGID files', 'outputs': SGID})
    dangerousfiles.append({'name': 'World writable SUID files', 'outputs': World_SUID})


def important_data(outscript):

    print(Colors.GREEN, "Attempting to access important files in root", Colors.END)

    try:
        shf = open('/etc/shadow', 'r').read()
        print(Colors.GREEN, "Reading the shadow file", Colors.END)
        verbint(shf)
        outscript.append({'name': 'ShadowFile', 'output': shf})
    except PermissionError:
        pass

    try:
        f = open('/etc/sudoers', 'r').read()
        print(Colors.GREEN, "Reading the sudoers file", Colors.END)
        verbint(f)
        outscript.append({'name': 'sudoers', 'output':  f})
    except PermissionError:
        pass

    try:

        inhalt = os.listdir('/root')
        print(Colors.GREEN, "Reading the content of the /root directory", Colors.END)
        verbint(inhalt)
        outscript.append({'name': 'Root directory', 'outputs': inhalt})
    except PermissionError:
        pass

    os.chdir('/')

    print(Colors.GREEN, "Gathering information about the system wide cron jobs", Colors.END)
    cronjobs_writable = []
    for path in ['/etc/cron.hourly', '/etc/cron.daily', '/etc/cron.weekly', '/etc/cron.monthly',
                 '/var/spool/cron/crontabs']:

        for dpath, dnames, fnames in os.walk(path):
            files = []
            for i, fname in enumerate([os.path.join(dpath, fname) for fname in fnames]):
                try:
                    f = open(fname).read()
                    files.append({'name': fname, 'output': f})
   
                    verbint("Checking if", fname, "is writable")

                    if (os.lstat(fname).st_mode & stat.S_ISUID) == 2048 and (
                                    (os.lstat(fname).st_mode & stat.S_IWOTH) != 0):
                        cronjobs_writable.append({'name': fname, 'output': f})

                except (PermissionError, FileNotFoundError):
                    outscript.append({'name': path, 'containers': files})
                    continue
            outscript.append({'name': path, 'containers': files})

    outscript.append({'name': 'World writable cronjobs', 'containers': cronjobs_writable})


    config_files = ['/etc/profile', '/etc/bashrc', '~/.bash_profile', '~/.bashrc', '~/.bash_logout']

    for path in config_files:
        print(Colors.GREEN, "reading ", path, Colors.END)
        try:
            b = open(path, 'r').read()
            verbint(path + ":")
            verbint(b)
            outscript.append({'name': path, 'output': b})
        except (FileNotFoundError, PermissionError):
            continue


    print("Reading the content of /tmp")
    out = os.listdir('/tmp')
    verbint(out)
    outscript.append({'name': 'tmp', 'outputs': out})

    for path in ['/etc/init.d/', '/etc/rc.d/', '/etc/xinetd.d']:
        try:
            files = []
            for file in os.listdir(path):
                print("Checking if:", os.path.join(path, file), "is writable")
                if os.stat(os.path.join(path, file)).st_mode & stat.S_IWOTH:
                    verbint(os.path.join(path, file), "is writable")
                    files.append({'name': os.path.join(path, file), 'output': ''.join([os.path.join(path, file), " is world - writable"])})
                    interesting_facts.append(''.join([os.path.join(path, file), "is writable"]))
                else:
                    files.append({'name': os.path.join(path, file),
                                  'output': ''.join([os.path.join(path, file), " is not world - writable"])})

            outscript.append({'name': path, 'containers': files})
        except FileNotFoundError:
            pass

    try:
        shells = []
        for line in open('/etc/shells').readlines():
            lex = shlex.shlex(line)
            line = ''.join(list(lex))
            if not line:
                continue
            shells.append(line)
        outscript.append({'name': 'Shells', 'outputs': shells})
    except (FileNotFoundError, PermissionError):
        pass

    print(Colors.GREEN, 'Looking for installed packages:', Colors.END)

    outjson = {'general': outscript}
    try:
        with open((original_path + '/ImportantData.json'), 'w') as outfile:
            json.dump(outjson, outfile)
    except PermissionError:
        pass

    post('/Pysec/ImportantData', outjson)


def network_info():

    networking = dict()

    ssh_paths = ['~/.ssh/authorized_keys', '~/.ssh/identity.pub', '~/.ssh/identity', '~/.ssh/id_rsa.pub',
                 '~/.ssh/id_rsa', '~/.ssh/id_dsa.pub', '~/.ssh/id_dsa', '/etc/ssh/ssh_config', '/etc/ssh/sshd_config',
                 '/etc/ssh/ssh_host_dsa_key.pub', '/etc/ssh/ssh_host_dsa_key', '/etc/ssh/ssh_host_rsa_key.pub',
                 '/etc/ssh/ssh_host_rsa_key', ' /etc/ssh/ssh_host_key.pub', '/etc/ssh/ssh_host_key',
                 '~/.ssh/known_hosts']
    networks = []
    for path in ssh_paths:
        print(Colors.GREEN, "Reading ", path, Colors.END)
        try:
            b = open(path, 'r').read()
            verbint(path + ":")
            verbint(b)
            networks.append({'name': path, 'output': b})
        except (FileNotFoundError, PermissionError):
            continue
    networking['SSH'] = {'name': 'SSH', 'containers': networks}

    print(Colors.GREEN, "Checking whether services are handled through xinetd.d or init.d", Colors.END)
    try:
        networks = []
        for path in ['/etc/init.d/', '/etc/rc.d/', '/etc/xinetd.d']:
            services = []

            for dpath, dnames, fnames in os.walk(path):
                for i, fname in enumerate([os.path.join(dpath, fname) for fname in fnames]):
                    try:
                        if path == '/etc/xinetd.d':
                            xin(fname)
                        services.append(
                            {'name': fname, 'output': open(fname).read()})
                    except (PermissionError, FileNotFoundError, UnicodeDecodeError):
                        continue
            networks.append({'name': path, 'containers': services})
    except (PermissionError, FileNotFoundError, UnicodeDecodeError):
        pass
    networking['ServiceManagement'] = {'name': 'serviceManagement', 'containers': networks}

    certificates = []
    print(Colors.GREEN, "Gathering information about the SSL certifications", Colors.END)
    for dpath, dnames, fnames in os.walk('/etc/ssl/certs'):
        for i, fname in enumerate([os.path.join(dpath, fname) for fname in fnames]):
            try:
                certificates.append({'name': fname, 'output': open(os.path.join('/etc/ssl/certs', fname)).read()})
            except (PermissionError, FileNotFoundError, UnicodeDecodeError):
                continue
    networking['certificates'] = {'name': 'certificates', 'containers': certificates}

    try:
        print(Colors.GREEN, "Gathering network information", Colors.END)
        out = subprocess.check_output(['ip', 'address']).decode("utf-8")
        verbint(out)
        networking['General'] = out
    except (FileNotFoundError, PermissionError):
        pass

    try:
        print(Colors.GREEN, "Reading the content of /etc/services", Colors.END)
        out = open('/etc/services', 'r').read()
        verbint(out)
        networking['services'] = out
    except (FileNotFoundError, PermissionError):
        pass

    post("/Pysec/Networking", networking)


def xin(fname):
    port = ""
    name = ""
    for line in open(fname).readlines():
        lexer = shlex.shlex(line)
        lexer.wordchars += './'
        token_list = []
        for token in lexer:
            token_list.append(token)
        if token_list == []:
            continue
        elif token_list[0] in ['log_on_failure']:
            continue
        elif token_list[0] in ['{', '}', 'socket_type', 'wait', 'bind', 'type', 'instances']:
            continue
        elif len(token_list) == 2 and token_list[0] == 'service':
            name = token_list[1]
        elif token_list[0] == 'port':
            port = ' '.join(token_list[2:])
    if len(port) > 0:
        xin_pots[port] = name


if __name__ == '__main__':

    
    parser = argparse.ArgumentParser(description='PYSEC security scanning')
    parser.add_argument("-s", "--standard", help="make a quick system scan", action="store_true")
    parser.add_argument("-f", "--full", help="make a full system scan", action="store_true")
    parser.add_argument("address", help="address of the web server", nargs='?', type=str)
    parser.add_argument("-v", "--verbose", help="increase the output of the verbosity", action="store_true")
    args = parser.parse_args()
    if args.verbose:
        def verbint(*args):
            for arg in args:
                print(arg,end="")
            print()
    else:
        verbint = lambda *a: None
    if args.address:
        def post(pysec, file):
            try:
                connection = http.client.HTTPConnection(args.address)
                connection.request('POST', pysec, json.dumps(file), headers)
                if connection.getresponse().read().decode() != 'Served at: /Pysec':
                    verbint(Colors.RED, "Couldn't post on http://" + args.address + pysec, Colors.END)
            except (ConnectionRefusedError):
                verbint(Colors.RED, "Couldn't post on http://" + args.address + pysec, Colors.END)
    else:
        post = lambda *a: None

    dangerousdata = []
    if args.full:
        t1 = threading.Thread(target=dangerous_data, args=[dangerousdata, 20])
    else:
        t1 = threading.Thread(target=dangerous_data, args=[dangerousdata, 2])
    print(Colors.PURPLE, "Scan started", Colors.END)
    t1.start()
    network_info()
    general_info()
    kernel_info()
    processes_info_linux()
    print("Currently scanning the system for files with SUID set")
    t1.join()
    user_info()
    important_data(dangerousdata)

    outscript = {
        'name': 'Important Data',
        'outputs': interesting_facts
    }
    try:
        with open((original_path + '/InterestingData.json'), 'w') as outfile:
            json.dump(outscript, outfile)
    except PermissionError:
        pass

    post("/Pysec/Interesting", outscript)

    print(Colors.PURPLE, "Scan completed", Colors.END)
