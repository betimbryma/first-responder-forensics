Linux/Unix "first responder forensics"
------------------------------------

**Goal:** 
Python script that extracts as much information as possible from the currently running *Linux* system.
The script should be able to execute with **or** without root privileges.
Without root privileges it will of course be limited in its capabilities.
The script itself does *not* have to operate "stealthy" i.e. have a low memory footprint.

The results of this script can later be compared with other analysis e.g. memory forensic either from outside a VM or directly on the cold-boot dump. 

## Tasks 
* Find obvious vulnerabilities on the system (why? those might have been already used for attacks or are backdoors)
	+ security and privilege escalation attack vectors (Read up on those to get started/into the topic)
    + History of local privilege escalation exploits
        - What happened in the past?
        - What is still possible? (e.g. in case of configuration/coding errors?)
		- [ ] Prepare a list on possible attack vectors e.g.
			-  cron jobs executing writeable scripts as root/other user
			-  wrong file/directory permissions
			- ... 
    + Links to other tools that might help:
        * http://pentestmonkey.net/tools/audit/unix-privesc-check
        * https://github.com/rebootuser/LinEnum
        * http://www.chkrootkit.org/
        * http://rkhunter.sourceforge.net/
        * http://www.trapkit.de/tools/checksec.html

* [ ] Extract as much information on running processes and kernel modules as possible. 
    + What is running on the system
    + Which files do processes use
    + What other resources? ( open ports, sockets, pipes, etc...)

* [ ] running kernel and config
    - .config of current kernels
    - which options are set/not set?
    - Protections (ASLR,DEP,...whatever else) turned on?

* [ ] users and system rights
    - Users on the systems
        * And which permissions do they have?
        * e.g. write to directories where they should not have write access (/usr/bin)
        * Default limits for users (quota)
        * ssh authorized keys set?
        * info in bashrc and similar config files? (e.g. PATH variable)
    - Which processes belong to which users
    - Are the interdependencies between users?
        * Access to the same files/folders?
        * Can one process influence another? (Cross service exploits in CTFs)

* [ ] cronjobs/System services (systemd)
    - Which jobs are running?
    - Can be combined with info from "running processes and kernel modules"

* [ ] tmp
    - What is in tmp?
    - How are rights set?
    - Again cross access for different users?
    - Do services use fixed tmp file names?

* [ ] installed software/binaries
    - What is installed?
    - Setuid set (other permission problems)
    - Known vulnerabilities/exploits for binaries in online databases? (CVE, etc.)


A paper on Privilege Escalation, and a server together with a GUI client that receives the results of the python script and displays them categorized on the nice user interface.

