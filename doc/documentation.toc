\contentsline {section}{\numberline {1}Introduction}{4}
\contentsline {section}{\numberline {2}Server}{5}
\contentsline {subsection}{\numberline {2.1}Waiting for client}{5}
\contentsline {section}{\numberline {3}Home}{5}
\contentsline {section}{\numberline {4}User info}{7}
\contentsline {subsection}{\numberline {4.1}Cracking password}{8}
\contentsline {section}{\numberline {5}.config file}{10}
\contentsline {section}{\numberline {6}Important data}{11}
\contentsline {subsection}{\numberline {6.1}Ports}{12}
\contentsline {subsection}{\numberline {6.2}Pipes}{12}
\contentsline {subsection}{\numberline {6.3}SUID}{13}
\contentsline {subsection}{\numberline {6.4}cron}{15}
\contentsline {section}{\numberline {7}Process info}{16}
\contentsline {section}{\numberline {8}Kernel info}{18}
\contentsline {subsection}{\numberline {8.1}Block kexec}{18}
\contentsline {subsection}{\numberline {8.2}Stack protector}{18}
\contentsline {subsection}{\numberline {8.3}Syscall Filtering}{19}
\contentsline {subsection}{\numberline {8.4}dev/mem protection}{19}
\contentsline {subsection}{\numberline {8.5}Read-only data sections}{20}
\contentsline {subsection}{\numberline {8.6}Module RO/NX}{20}
\contentsline {subsection}{\numberline {8.7}Address 0 protection}{20}
\contentsline {subsection}{\numberline {8.8}SMACK}{20}
\contentsline {subsection}{\numberline {8.9}/dev/kmem}{20}
\contentsline {subsection}{\numberline {8.10}Booted in}{21}
\contentsline {subsection}{\numberline {8.11}Blacklist.conf}{21}
\contentsline {subsection}{\numberline {8.12}SELinux}{21}
\contentsline {subsection}{\numberline {8.13}Kernel Address Display Restriction}{21}
\contentsline {subsection}{\numberline {8.14}dmesg restrictions}{21}
\contentsline {subsection}{\numberline {8.15}Kernel Address Space Layout Randomisation}{22}
\contentsline {subsection}{\numberline {8.16}Ptrace}{22}
\contentsline {subsection}{\numberline {8.17}Symlink restrictions}{22}
\contentsline {subsection}{\numberline {8.18}Hardlink restrictions}{22}
\contentsline {subsection}{\numberline {8.19}SYN coockies protection}{22}
\contentsline {section}{\numberline {9}Networking}{23}
\contentsline {subsection}{\numberline {9.1}certificates}{23}
\contentsline {section}{\numberline {10}Pysec script}{24}
