/**
 * @author betim
 */
package gui;

import java.io.File;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application{

	
	public static void main(String[] args){
		launch(args);
		
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Main.fxml"));
		Parent root = loader.load();
		Controller controller = loader.getController();
		primaryStage.setTitle("PYSEC");
		Scene scene = new Scene(root);
		//Application.setUserAgentStylesheet(getClass().getResource("style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
		primaryStage.setOnCloseRequest(e -> {
			new File("passwd").delete();
			new File("shadow").delete();
		});
	}
	
}
