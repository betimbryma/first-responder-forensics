/**
 * @author betim
 */
package gui;

import com.google.gson.*;

import DTO.ConfigDTO;
import DTO.Container;
import DTO.GeneralDTO;
import DTO.ImportantDataDTO;
import DTO.KernelDTO;
import DTO.NetworkingDTO;
import DTO.ProcessInfoDTO;
import DTO.UserDTO;
import DTO.UserInformationDTO;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class Controller implements Initializable{
	
	@FXML
	private TreeView<Container> TreeConfig, TreeImportant, TreeUser, TreeNetworking, TreeKernel;
	@FXML
	private ListView<Container> generalList;
	@FXML
	private ListView<UserDTO> userList;
	@FXML
	private ListView<ProcessInfoDTO> processList;
	@FXML
	private ScrollPane  ScrollConfig, ScrollImportant, ScrollProcess, ScrollKernel, groupsScroll,
		filesScroll, pipesScroll, portsScroll, suidScroll, sgidScroll;
	@FXML
	private TextField searchBar;
	@FXML
	private Button cancelButton, passwordButton;
	@FXML
	private ImageView imageView;
	@FXML
	private Label configLabel, importantLabel, processLabel, kernelLabel,  
		userLabel, uidLabel, gidLabel, cronjobsLabel, hdirLabel, homedirLabel,
		generalName, systemName, userGeneral, idGeneral, releaseGeneral, distGeneral,
		groupsLabel, generalPath, generalProcessor, generalMachine, InterestingLabel,
		processPID, processOwner, processName, processPath, dangerousProcess,
		processFiles, processPipes, processPorts, passwordLabel,
		networkingGeneral, networkingLabel, encryptedLabel, softwareLabel,
		suidLabel, sgidLabel;
	@FXML
	private Tab homeTab, userTab, configTab, importantTab, processTab, kernelTab, networkingTab, softwareTab;
	private List<String> ports;
	private HashMap<String, String> pipes;
	private Helper helper;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		helper = new Helper();
		
		pipes = new HashMap<String, String>();
		ports = new ArrayList<String>();
		
		homeTab.setGraphic(new Label("Home"));
		homeTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		userTab.setGraphic(new Label("User info"));
		userTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		configTab.setGraphic(new Label(".config file"));
		configTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		importantTab.setGraphic(new Label("Important data"));
		importantTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		processTab.setGraphic(new Label("Process info"));
		processTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		kernelTab.setGraphic(new Label("Kernel info"));
		kernelTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		networkingTab.setGraphic(new Label("Networking"));
		networkingTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		softwareTab.setGraphic(new Label("Software"));
		softwareTab.getGraphic().setStyle("-fx-text-fill: #ffcc00;");
		
		Networking();
		Config();
		General();
		ImportantData();
		Kernel();
		ProcessInfo();
		User();
		Important();
		
		Image image = new Image("resources/logo.png");
		imageView.setImage(image);
		cancelButton.setOnAction(e -> {
			Stage stage = (Stage) cancelButton.getScene().getWindow();
			stage.close();
		});
		
	}
	
	private void Networking(){
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){
			
			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8087);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				NetworkingDTO networkingDTO = new Gson().fromJson(stringProperty.getValue(), NetworkingDTO.class);
				
				TreeItem<Container> root = new TreeItem<>();
				TreeNetworking.setRoot(root);
				TreeNetworking.setShowRoot(false);
				Container certificates = networkingDTO.getCertificates();
				Container SSH = networkingDTO.getSSH();
				Container xinetd = networkingDTO.getServiceManagement();
				List<Container> containers = new ArrayList<Container>();
				String services = networkingDTO.getServices();
				if(certificates != null)
					containers.add(certificates);
				if(SSH != null)
					containers.add(SSH);
				if(xinetd != null)
					containers.add(xinetd);
				
				helper.populateRoot(root, containers);
				helper.makeBranch(new Container("services", services), root);
				String general = networkingDTO.getGeneral();
				if(general != null)
					networkingGeneral.setText(general);
				TreeNetworking.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
					if(newValue != null)
						networkingLabel.setText(newValue.getValue().getOutput());
				});
				
				networkingTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				networkingTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	
	}
	
	private void Config(){
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){
			
			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8081);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				ConfigDTO config = new Gson().fromJson(stringProperty.getValue(), ConfigDTO.class);
				
				configLabel.setText(config.getOutput());
				
				configTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				configTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	private void General(){
		StringProperty text = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){

			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8082);
						Socket server = serverSocket.accept();
						DataInputStream in = new DataInputStream(server.getInputStream());
						updateMessage(in.readUTF());
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){
			
			@Override
			public void handle(WorkerStateEvent event) {
				GeneralDTO general = new Gson().fromJson(text.getValue(), GeneralDTO.class);
				generalName.setText(general.getName());
				systemName.setText(general.getSystem());
				releaseGeneral.setText(general.getRelease());
				distGeneral.setText(general.getDist());
				generalMachine.setText(general.getMachine());
				generalProcessor.setText(general.getProcessor());
				generalPath.setText(general.getPath());
				String user = general.getUser();
				if(Integer.parseInt(general.getID()) != 0)
					user += " -non-root user";
				else
					user += " -root user";
				userGeneral.setText(user);
				idGeneral.setText(general.getID());
				
				text.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				homeTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		text.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	private void ImportantData(){
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){

			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8083);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				ImportantDataDTO general = new Gson().fromJson(stringProperty.getValue(), ImportantDataDTO.class);
				
				List<Container> generals = general.getGeneral();
				
				TreeImportant.setShowRoot(false);
				TreeItem<Container> root;
				if(TreeImportant.getRoot() == null){
					root = new TreeItem<>();
					TreeImportant.setRoot(root);
				} else 
					root = TreeImportant.getRoot();
				root.setExpanded(true);
				if(generals != null)
					helper.populateRoot(root, generals);
				
				TreeImportant.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
					if(newValue != null)
						importantLabel.setText(newValue.getValue().getOutput());
				});
				importantTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				importantTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	
	private void Kernel(){
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){

			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8084);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
						
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				KernelDTO kernel = new Gson().fromJson(stringProperty.getValue(), KernelDTO.class);
				
				if (kernel == null){
					kernelTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
					return;
				}
				List<Container> container = kernel.getGeneral();
				String software = kernel.getSoftware();
				StringBuilder stringBuilder = new StringBuilder();
				if(software != null){
					String packages[] = software.split("\n");
					for(String line : packages){
						if(line.startsWith("|") || line.startsWith("+"))
							continue;
						String after = line.trim().replaceAll(" +", " ");
						stringBuilder.append(after);
						stringBuilder.append(System.lineSeparator());
					}
					softwareLabel.setText(stringBuilder.toString());
					softwareTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				} else 
					softwareTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				TreeItem<Container> root;
				if(TreeKernel.getRoot() == null){
					root = new TreeItem<>();
					TreeKernel.setRoot(root);
				} else 
					root = TreeKernel.getRoot();
				
				TreeKernel.setShowRoot(false);
				helper.populateRoot(root, container);
				TreeKernel.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> {
					if(newValue != null)
						kernelLabel.setText(newValue.getValue().getOutput());
				});
				kernelTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				kernelTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	private void Important(){
		
		StringProperty stringProperty = new SimpleStringProperty();
		
		Service<Void> backgroundThread = new Service<Void>(){
		
			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8088);
						Socket server = serverSocket.accept();
						DataInputStream in = new DataInputStream(server.getInputStream());
						
						
						updateMessage(in.readUTF());
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){	
			@Override
			public void handle(WorkerStateEvent event) {
				Container container = new Gson().fromJson(stringProperty.getValue(), Container.class);
				StringBuilder stringBuilder = new StringBuilder();
				for(String output : container.getOutputs()){
					stringBuilder.append(output);
					stringBuilder.append(System.lineSeparator());
				}
				InterestingLabel.setText(stringBuilder.toString());
				homeTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				homeTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
	}
	
	private void ProcessInfo(){
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){

			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8085);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				ProcessInfoDTO[] processes = new Gson().fromJson(stringProperty.getValue(), ProcessInfoDTO[].class);
				for(ProcessInfoDTO process : processes){
					
					ports.addAll(process.getPorts());
					
					for(String string : process.getPipes()){						
						if(pipes.containsKey(string))
							pipes.put(string, pipes.get(string)+", "+process.toString());
						else
							pipes.put(string, string+": "+process.toString());
					}
					
				}
				TreeItem<Container> root;
				if(TreeImportant.getRoot() == null){
					root = new TreeItem<>();
					TreeImportant.setRoot(root);
				} else 
					root = TreeImportant.getRoot();
				TreeItem<Container> port = helper.makeBranch(new Container("Ports", "Different ports that were being used in the time of scanning"), root);
				for(String file : ports)
					helper.makeBranch(new Container(file.split(" ")[0], file), port);
				TreeItem<Container> pipe = helper.makeBranch(new Container("Pipes", "Different pipes that were being used in the time of scanning"), root);
				for(Map.Entry<String, String> entry : pipes.entrySet())
					helper.makeBranch(new Container(entry.getKey(), entry.getValue()), pipe);
				ObservableList<ProcessInfoDTO> processesObserved = FXCollections.observableArrayList();
				processesObserved.addAll(Arrays.asList(processes));
				processList.setItems(processesObserved);
				
				
				processList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ProcessInfoDTO>(){
					
					@Override
					public void changed(ObservableValue<? extends ProcessInfoDTO> observable, ProcessInfoDTO oldValue,
							ProcessInfoDTO newValue) {
						
						processPID.setText(newValue.getPID());
						processOwner.setText(newValue.getOwner());
						processName.setText(newValue.getName());
						processPath.setText(newValue.getPath());
						dangerousProcess.setText(newValue.getDangerous());
						processFiles.setText(newValue.getFiles().toString().replace(",", "\n").replace("]", "").replace("[", "").trim());
						StringBuilder stringBuilder = new StringBuilder();
						for(String pipe : newValue.getPipes())
							stringBuilder.append(pipes.get(pipe)+"\n");
							
						
						processPipes.setText(stringBuilder.toString());	
						processPorts.setText(newValue.getPorts().toString().replace(",", "\n").replace("]", "").replace("[", "").trim());
					}});
				portsScroll.setOnMouseClicked(e -> {
					
					ProcessInfoDTO	process = processList.getSelectionModel().getSelectedItem();
					if(process!=null && !processPorts.getText().isEmpty())
						helper.popUp(process.getName()+"'s ports", processPorts.getText());
			
				});
				pipesScroll.setOnMouseClicked(e -> {
					
					ProcessInfoDTO	process = processList.getSelectionModel().getSelectedItem();
					if(process!=null && !processPipes.getText().isEmpty())
						helper.popUp(process.getName()+"'s pipes", processPipes.getText());
			
				});
				filesScroll.setOnMouseClicked(e -> {
	
					ProcessInfoDTO	process = processList.getSelectionModel().getSelectedItem();
					if(process!=null && !processFiles.getText().isEmpty())
						helper.popUp(process.getName()+"'s files", processFiles.getText());

				});
				processTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				processTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	private void User(){
		passwordButton.setDisable(true);
		StringProperty stringProperty = new SimpleStringProperty();
		Service<Void> backgroundThread = new Service<Void>(){

			@Override
			protected Task<Void> createTask() {
				
				return new Task<Void>(){

					@Override
					protected Void call() throws Exception {
						
						ServerSocket serverSocket = new ServerSocket(8086);
						Socket server = serverSocket.accept();
						InputStream is = server.getInputStream();
						DataInputStream dis = new DataInputStream(is);
						int len = dis.readInt();
						byte[] buff = new byte[len];
						dis.readFully(buff);
						String response = new String(buff, "UTF-8");
						updateMessage(response);
						server.close();
						serverSocket.close();
						return null;
					
					}};
			}
			
		};
		backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){	
			@Override
			public void handle(WorkerStateEvent event) {
				UserInformationDTO userInfo = new Gson().fromJson(stringProperty.getValue(), UserInformationDTO.class);
				ObservableList<UserDTO> containers = FXCollections.observableArrayList();
				String shadow = userInfo.getShadow();
				String passwd = userInfo.getPasswd();
				Map<String, String> passwords = new HashMap<>();
				
				
				if(!shadow.isEmpty() && !passwd.isEmpty()){
					String algorithm = "The password are stored using the ";
					loop: for(String pass : shadow.split("\n")){
							String hashpass = pass.split(":")[1];
							if(!hashpass.isEmpty()){
								
								String algo = hashpass.split("\\$")[1];
								
								switch(algo){
									case "1":
										algorithm += "MD5 hashing algorithm";
										break loop;
									case "2":
										algorithm += "Blowfish Algorithm";
										break loop;
									case "2a":
										algorithm += "eksblowfish Algorithm";
										break loop;
									case "5":
										algorithm += "SHA-256 Algorithm";
										break loop;
									case "6":
										algorithm += "SHA-512 Algorithm";
										break loop;
									default:
										break;
									
								}
							}
						}
					TreeItem<Container> root;
					if(TreeKernel.getRoot() == null){
						
						root = new TreeItem<>();
						TreeKernel.setRoot(root);
					} else 
						root = TreeKernel.getRoot();
					helper.makeBranch(new Container("Hashing algorithm", algorithm), root);
					
					
					
					try{
						
						try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					              new FileOutputStream("shadow"), "utf-8"))) {
					    writer.write(shadow);}
						try (Writer writer = new BufferedWriter(new OutputStreamWriter(
					              new FileOutputStream("passwd"), "utf-8"))) {
					    writer.write(passwd);}
						Runtime r = Runtime.getRuntime();
						Process p = r.exec(new String[]{"unshadow", "passwd", "shadow"});
						p.waitFor();
						BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
						String line = "";
						
						while((line = b.readLine()) != null){
							if(!line.split(":")[1].equals("*"))
								passwords.put(line.split(":")[0], line);	
						}
						
					} catch (IOException | InterruptedException io){
						
						io.printStackTrace();
					}
					}
				
				containers.addAll(userInfo.getUsers());
				
				userList.setItems(containers);
			
				userList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<UserDTO>(){

					@Override
					public void changed(ObservableValue<? extends UserDTO> observable, UserDTO oldValue,
							UserDTO newValue) {
						userLabel.setText(newValue.getUser());
						uidLabel.setText(newValue.getUID());
						gidLabel.setText(newValue.getGID());
						homedirLabel.setText(newValue.getHDIR());
						groupsLabel.setText(newValue.getGROUPS().toString().replace(",", "\n").replace("]", "").replace("[", "").trim());
						suidLabel.setText(newValue.getSUID().toString().replace(",", "\n").replace("]", "").replace("[", "").trim());
						sgidLabel.setText(newValue.getSGID().toString().replace(",", "\n").replace("]", "").replace("[", "").trim());
						cronjobsLabel.setText(newValue.getCronjobs());
						if(newValue.isEncrypted()){
							encryptedLabel.setText("encrypted");
							encryptedLabel.setStyle("-fx-text-fill: #5190b8;");
						} else {
							encryptedLabel.setText("unencrypted");
							encryptedLabel.setStyle("-fx-text-fill: #cc0000;");
						}
						if(newValue.getPassword() == null && passwords.get(newValue.getUser()) != null){
							passwordButton.setDisable(false);
							passwordLabel.setText(passwords.get(newValue.getUser()));
							
						} else if (newValue.getPassword()!=null){
							passwordButton.setDisable(true);
							passwordLabel.setText(newValue.getPassword());
						}
						else{
							if(userInfo.getShadow().isEmpty())
								passwordLabel.setText("Could not obtain the password");
							else
								passwordLabel.setText("No password");
							passwordButton.setDisable(true);
						}
						
					}});
				groupsScroll.setOnMouseClicked(e -> {
					UserDTO user = userList.getSelectionModel().getSelectedItem();
					if(user != null && !groupsLabel.getText().isEmpty())
						helper.popUp(user.getUser()+"'s groups", groupsLabel.getText());
				});
				suidScroll.setOnMouseClicked(e -> {
					UserDTO user = userList.getSelectionModel().getSelectedItem();
					if(user != null && !suidLabel.getText().isEmpty())
						helper.popUp(user.getUser()+"'s SUID files", suidLabel.getText());
				});
				sgidLabel.setOnMouseClicked(e -> {
					UserDTO user = userList.getSelectionModel().getSelectedItem();
					if(user != null && !sgidLabel.getText().isEmpty())
						helper.popUp(user.getUser()+"'s SGID files", sgidLabel.getText());
				});
				userTab.getGraphic().setStyle("-fx-text-fill: #33cc33;");
				stringProperty.unbind();
			}});
		backgroundThread.setOnFailed(new EventHandler<WorkerStateEvent>(){

			@Override
			public void handle(WorkerStateEvent event) {
				
				userTab.getGraphic().setStyle("-fx-text-fill: #cc0000;");
				
			}
			
		});
		stringProperty.bind(backgroundThread.messageProperty());
		backgroundThread.restart();
		
	}
	
	
	@FXML
	private void passwordButtonPressed(){
		UserDTO userDTO = userList.getSelectionModel().getSelectedItem();
		
		if(userDTO != null && userDTO.getPassword() == null){
			Stage stage = new Stage();
			Label labelTop = new Label("Trying to crack "+userDTO.getUser()+"'s password");
			labelTop.setTextFill(Color.color(0.5598, 0.2174, 0.0000, 0.2784));
			
			labelTop.setStyle("-fx-font: 19 Ubuntu;");
			BorderPane borderPane = new BorderPane();
			Label label = new Label();
			label.setStyle("-fx-font: 19 Ubuntu;");
			borderPane.setTop(labelTop);
			borderPane.setCenter(label);
            Scene scene = new Scene(borderPane, 400, 250);
            stage.setScene(scene);
            stage.setTitle("John the ripper");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
            Service<Void> backgroundThread = new Service<Void>(){

				@Override
				protected Task<Void> createTask() {
					
					return new Task<Void>(){

						@Override
						protected Void call() throws Exception {
							try{
								if(!passwordLabel.getText().isEmpty()){
									try (Writer writer = new BufferedWriter(new OutputStreamWriter(
								              new FileOutputStream(userDTO.getUser()), "utf-8"))) {
								    writer.write(passwordLabel.getText());}
									Runtime r = Runtime.getRuntime();
									Process p = r.exec(new String[]{"john", userDTO.getUser()});
									p.waitFor();
									BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
									String line = "";
									
									while((line = b.readLine()) != null){
										Pattern pattern = Pattern.compile("(.*?)(\\s{10})(\\("+userDTO.getUser()+"\\))");
										Matcher matcher = pattern.matcher(line);
										
										if(matcher.find())
											updateMessage(matcher.group(1));
						
										
									}
									
								}} catch(IOException | InterruptedException io){
									io.printStackTrace();
								}
							return null;
						}
						
					};
				}
            	
            };
            backgroundThread.setOnSucceeded(new EventHandler<WorkerStateEvent>(){

				@Override
				public void handle(WorkerStateEvent event) {
					
					userDTO.setPassword(label.getText());
					
					passwordLabel.setText(label.getText());
					passwordButton.setDisable(true);
					new File(userDTO.getUser()).delete();
				}
            	
            });
            label.textProperty().bind(backgroundThread.messageProperty());
            
    		backgroundThread.restart();
			
    		stage.setOnCloseRequest(e -> {
    			backgroundThread.cancel();
    		});
			
		}
	}
	
	
}
