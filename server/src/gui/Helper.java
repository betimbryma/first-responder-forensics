package gui;

import java.util.List;

import DTO.Container;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.stage.Stage;

public class Helper {
	
	public void populateRoot(TreeItem<Container> root, List<Container> containers){
		for(Container container : containers){
			if(container.getOutput() != null)
				makeBranch(container, root);
			else if (container.getOutputs() != null){
				TreeItem<Container> item = makeBranch(new Container(container.getName(), container.getName()), root);
				for(String output : container.getOutputs()){
					makeBranch(new Container(output, output), item);
				}
			}
			else if (container.getContainers() != null){
				TreeItem<Container> item = makeBranch(new Container(container.getName(), container.getName()), root);
				populateRoot(item, container.getContainers());
			}
		}
	}
	
	public TreeItem<Container> makeBranch(Container container, TreeItem<Container> parent){
		TreeItem<Container> item = new TreeItem<>(container);
		item.setExpanded(false);
		parent.getChildren().add(item);
		return item;
	}
	
	public void popUp(String title, String content){
		Label label = new Label();
		label.setStyle("-fx-font: 19 Ubuntu;");
		label.setText(content);
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(label);
		Scene scene = new Scene(scrollPane, 300, 300);
		Stage stage = new Stage();
		stage.setScene(scene);
		stage.setTitle(title);
		stage.show();
		
	}

}
