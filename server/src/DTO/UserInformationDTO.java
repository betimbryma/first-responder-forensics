/**
 * @author betim
 */
package DTO;

import java.util.List;

public class UserInformationDTO {
	
	private String shadow;
	private String passwd;
	List<UserDTO> users;
	
	public String getShadow() {
		return shadow;
	}
	public void setShadow(String shadow) {
		this.shadow = shadow;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public List<UserDTO> getUsers() {
		return users;
	}
	public void setUsers(List<UserDTO> users) {
		this.users = users;
	}
	
	
}
