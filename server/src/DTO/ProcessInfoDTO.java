
/**
 * @author betim
 */
package DTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ProcessInfoDTO {
	
	private String PID;
	private String Owner;
	private String Name;
	private String Path;
	private String Dangerous;
	private List<String> Files;
	private List<String> Ports;
	private List<String> Pipes;
	
	public ProcessInfoDTO(String owner, String name, String path, String dangerous, List<String> files) {
		
		Owner = owner;
		Name = name;
		Path = path;
		Dangerous = dangerous;
		Files = files;
	}

	public List<String> getPorts() {
		return Ports;
	}

	public void setPorts(List<String> ports) {
		Ports = ports;
	}
	
	public List<String> getPipes() {
		return Pipes;
	}

	public void setPipes(List<String> pipes) {
		Pipes = pipes;
	}

	public String getPID() {
		return PID;
	}

	public void setPID(String pID) {
		PID = pID;
	}

	public List<String> getFiles() {
		return Files;
	}

	public void setFiles(List<String> files) {
		Files = files;
	}

	public String getOwner() {
		return Owner;
	}
	public void setOwner(String owner) {
		Owner = owner;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getPath() {
		return Path;
	}
	public void setPath(String path) {
		Path = path;
	}
	public String getDangerous() {
		return Dangerous;
	}
	public void setDangerous(String dangerous) {
		Dangerous = dangerous;
	}
	
	@Override
	public String toString(){
		return PID+":"+this.Name;
	}
	
	
	
}
