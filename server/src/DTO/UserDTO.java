/**
 * @author betim
 */
package DTO;

import java.util.List;

public class UserDTO {
	
	private String User;
	private String UID;
	private String GID;
	private String HDIR;
	private List<String> GROUPS;
	private boolean Writable;
	private String Cronjobs;
	private String password;
	private String hashedPassword;
	private boolean Encrypted;
	private List<String> SUID;
	private List<String> SGID;
	
	
	public List<String> getSUID() {
		return SUID;
	}
	public void setSUID(List<String> sUID) {
		SUID = sUID;
	}
	public List<String> getSGID() {
		return SGID;
	}
	public void setSGID(List<String> sGID) {
		SGID = sGID;
	}
	public boolean isEncrypted() {
		return Encrypted;
	}
	public void setEncrypted(boolean encrypted) {
		Encrypted = encrypted;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHashedPassword() {
		return hashedPassword;
	}
	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}
	public String getUser() {
		return User;
	}
	public void setUser(String user) {
		User = user;
	}
	public String getUID() {
		return UID;
	}
	public void setUID(String uID) {
		UID = uID;
	}
	public String getGID() {
		return GID;
	}
	public void setGID(String gID) {
		GID = gID;
	}
	public String getHDIR() {
		return HDIR;
	}
	public void setHDIR(String hDIR) {
		HDIR = hDIR;
	}
	
	public List<String> getGROUPS() {
		return GROUPS;
	}
	public void setGROUPS(List<String> gROUPS) {
		GROUPS = gROUPS;
	}
	public boolean isWritable() {
		return Writable;
	}
	public void setWritable(boolean writable) {
		Writable = writable;
	}
	public String getCronjobs() {
		return Cronjobs;
	}
	public void setCronjobs(String cronjobs) {
		Cronjobs = cronjobs;
	}
	@Override
	public String toString(){
		return this.User;
	}
	
}
