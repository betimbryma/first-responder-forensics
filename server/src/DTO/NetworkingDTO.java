package DTO;

import java.util.List;

public class NetworkingDTO {
	
	private Container certificates;
	private Container ServiceManagement;
	private String services;
	private String General;
	private Container SSH;
	
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
	public String getGeneral() {
		return General;
	}
	public void setGeneral(String general) {
		General = general;
	}
	public Container getCertificates() {
		return certificates;
	}
	public void setCertificates(Container certificates) {
		this.certificates = certificates;
	}
	public Container getServiceManagement() {
		return ServiceManagement;
	}
	public void setServiceManagement(Container serviceManagement) {
		ServiceManagement = serviceManagement;
	}
	public Container getSSH() {
		return SSH;
	}
	public void setSSH(Container sSH) {
		SSH = sSH;
	}
	
	
}
