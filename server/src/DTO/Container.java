/**
 * @author betim
 */
package DTO;

import java.util.List;

public class Container {
	private String name;
	private String output;
	private String[] outputs;
	private List<Container> containers;
	
	public Container(String name, String output){
		this.name=name;
		this.output=output;
	}
	
	
	public Container(String name, String[] outputs) {
		super();
		this.name = name;
		this.outputs = outputs;
	}




	public List<Container> getContainers() {
		return containers;
	}


	public void setContainers(List<Container> containers) {
		this.containers = containers;
	}


	public String[] getOutputs() {
		return outputs;
	}


	public void setOutputs(String[] outputs) {
		this.outputs = outputs;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	
	@Override
	public String toString(){
		return name;
	}
}
