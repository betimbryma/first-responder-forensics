/**
 * @author betim
 */
package DTO;

import java.util.List;

public class KernelDTO {
	
	private List<Container> general;
	private String software;

	public List<Container> getGeneral() {
		return general;
	}

	public void setGeneral(List<Container> general) {
		this.general = general;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}
	
	
	
}
