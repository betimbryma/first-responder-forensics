/**
 * @author betim
 */
package DTO;

import java.util.List;

public class ImportantDataDTO {
	
	private List<Container> general;
	private NetworkingDTO networking;
	
	public List<Container> getGeneral() {
		return general;
	}
	public void setGeneral(List<Container> general) {
		this.general = general;
	}
	public NetworkingDTO getNetworking() {
		return networking;
	}
	public void setNetworking(NetworkingDTO networking) {
		this.networking = networking;
	}
	
}
